//Veronika Pontolillo 2034373
import java.lang.*;

public class Vector3d {
    //fields
    private double x;
    private double y;
    private double z;

public Vector3d(double x, double y, double z) {
    this.x = x;
    this.y = y;
    this.z = z;
}

public double getX(){
    return this.x;
}

public double getY(){
    return this.y;
}

public double getZ(){
    return this.z;
}

public double magnitude(){

    double newMagnitude = Math.sqrt(Math.pow(this.x,2) + Math.pow(this.y,2) + Math.pow(this.z,2));
    return newMagnitude;
}
public double dotProduct(Vector3d vec){
    Vector3d vec4 = new Vector3d(4.0, 5.0, 6.0);
    double dotVec = (vec.getX()*vec4.getX()) + (vec.getY()*vec4.getY()) + (vec.getZ()*vec4.getZ());
    return dotVec;
}
public Vector3d add(Vector3d vec2){
    Vector3d vec3 = new Vector3d(1.0, 2.0, 3.0);
    double vecNew1 = (vec2.getX() + vec3.getX());
    double vecNew2 = (vec2.getY() + vec3.getY());
    double vecNew3 = (vec2.getZ() + vec3.getZ());
    Vector3d newVec = new Vector3d(vecNew1, vecNew2, vecNew3);
    return newVec;
}
}
