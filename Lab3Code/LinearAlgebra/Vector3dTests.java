//Veronika Pontolillo 2034373
import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
    @Test 
    //checking get methods
    public void checkGetMethods(){
        Vector3d v1Test = new Vector3d(1.0,1.0,2.0);
        assertEquals(1.0, v1Test.getX());
        assertEquals(1.0, v1Test.getY());
        assertEquals(2.0, v1Test.getZ());
    
    }

    @Test 
    //checking magnitude()
    public void checkMagnitude(){
        Vector3d v2Test = new Vector3d(0.0,0.0,2.0);
        assertEquals(2.0, v2Test.magnitude());
    }

    @Test 
    //checking dotProduct()
    public void checkDotProduct(){
        Vector3d v3Test = new Vector3d(1.0,1.0,2.0);
        Vector3d v4Test = new Vector3d(2.0,3.0,4.0);
        assertEquals(21.0, v3Test.dotProduct(v3Test));
        assertEquals(47.0, v4Test.dotProduct(v4Test));
    }

    @Test 
    //checking add()
    public void checkAdd(){
        Vector3d v5Test = new Vector3d(1.0,2.0,3.0);
        Vector3d v6Test = new Vector3d(4.0,6.0,7.0);
        assertEquals(2.0, v5Test.add(v5Test).getX());
        assertEquals(4.0, v5Test.add(v5Test).getY());
        assertEquals(6.0, v5Test.add(v5Test).getZ());
        assertEquals(5.0, v6Test.add(v6Test).getX());
        assertEquals(8.0, v6Test.add(v6Test).getY());
        assertEquals(10.0, v6Test.add(v6Test).getZ());
    }
}
